# -*- coding: utf-8 -*-
import numpy
from .state import State
import random
import datetime, time

class Controller:
    def __init__(self, game, load, state):
        self.initialize_parameters(game, load, state)

    def initialize_parameters(self, game, load,state):
        self.state = state
        self.melhorPerf = 0
        self.mediamelhorPerf = 0
        self.melhoresParam = []
        self.cont = 10
        self.ultimasPerf = 0
        self.mediaPerf = []
        for i in range(0, 24):
            self.melhoresParam.append(0)

        if load == None:
            self.parameters = numpy.random.normal(0, 1, 3*len(self.compute_features()))
        else:
            params = open(load, 'r')
            weights = params.read().split("\n")
            self.parameters = [float(x.strip()) for x in weights[0:-1]]


    def output(self, episode, performance):
       print "Performance do episodio #%d: %d" % (episode, performance)

#--------------------------------------------------------------------------------------------------------
    
    #FUNCAO A SER COMPLETADA. Deve utilizar os pesos para calcular as funções de preferência Q para cada ação e retorna
    #-1 caso a ação desejada seja esquerda, +1 caso seja direita, e 0 caso seja ação nula
    def take_action(self, State):
    
        pesos = []
        for i in range(0, 24):
            pesos.append(0)
        
        #self.features = self.compute_features
        self.features = self.compute_features(State)

        #QE:
        pesos[0] = self.parameters[0] * self.features[0]
        pesos[1] = self.parameters[1] * self.features[1]
        pesos[2] = self.parameters[2] * self.features[2]
        pesos[3] = self.parameters[3] * self.features[3]
        pesos[4] = self.parameters[4] * self.features[4]
        pesos[5] = self.parameters[5] * self.features[5]
        pesos[6] = self.parameters[6] * self.features[6]
        pesos[7] = self.parameters[7] * self.features[7]

        #Q0:
        pesos[8] = self.parameters[8] * self.features[0]
        pesos[9] = self.parameters[9] * self.features[1]
        pesos[10] = self.parameters[10] * self.features[2]
        pesos[11] = self.parameters[11] * self.features[3]
        pesos[12] = self.parameters[12] * self.features[4]
        pesos[13] = self.parameters[13] * self.features[5]
        pesos[14] = self.parameters[14] * self.features[6]
        pesos[15] = self.parameters[15] * self.features[7]

        #QD:
        pesos[16] = self.parameters[16] * self.features[0]
        pesos[17] = self.parameters[17] * self.features[1]
        pesos[18] = self.parameters[18] * self.features[2]
        pesos[19] = self.parameters[19] * self.features[3]
        pesos[20] = self.parameters[20] * self.features[4]
        pesos[21] = self.parameters[21] * self.features[5]
        pesos[22] = self.parameters[22] * self.features[6]
        pesos[23] = self.parameters[23] * self.features[7]

        QE = pesos[0]  + pesos[1] + pesos[2] + pesos[3] + pesos[4] + pesos[5] + pesos[6] + pesos[7]
        Q0 = pesos[8]  + pesos[9] + pesos[10] + pesos[11] + pesos[12] + pesos[13] + pesos[14] + pesos[15]
        QD = pesos[16] + pesos[17] + pesos[18] + pesos[19] + pesos[20] + pesos[21] + pesos[22] + pesos[23]
        
        #print "QE: %f Q0: %f QD: %f" % (QE, Q0, QD)
        
        Pi = max(QE, Q0, QD)
        #print "Pi: %f" % Pi

        if Pi == Q0:
            return 0 # Parado

        if Pi == QE:
            return -1 # Esquerda

        if Pi == QD:
            return 1 # Direita

    #FUNCAO A SER COMPLETADA. Deve calcular features expandidas do estados
    def compute_features(self, state):

       # Calcula melhor x possível:
        lastfeat = 1024*state.wheel_x - (state.wheel_x*state.wheel_x)
    
       # Computa as features:

        C1 = [  numpy.sin(state.wheel_x*(state.wheel_x-1)), 
                numpy.sin(state.rod_angle),
                1/(state.wheel_x + state.angular_velocity),
                state.wheel_x*state.angular_velocity, 
                state.rod_angle*state.rod_angle,
                state.velocity_x*(1-state.friction),
                state.wind*(1-state.friction),
                lastfeat]

        return C1


    #FUNCAO A SER COMPLETADA. Deve atualizar a propriedade self.parameters
    def update(self, episode, performance):
        
        #Hill Climbing Algorithm

        # Pega as dez ultimas performances e faz a media delas
        self.cont = self.cont - 1
        if(self.cont > 0):
                self.ultimasPerf = self.ultimasPerf + performance 
        else:
        # Faz a média das ultimas permances 
            self.ultimasPerf = self.ultimasPerf + performance 
            self.cont = 10
            self.mediaPerf = self.ultimasPerf
            self.ultimasPerf = 0

        # Verifica a media é melhor que a melhor perfomance
            if self.mediaPerf >= self.mediamelhorPerf:
	            for k in range(0,24):
	                self.melhoresParam[k] = self.parameters[k] 
	            self.mediamelhorPerf = self.mediaPerf

        # Testa se vizinho gerado é melhor que o anterior
	    #    if performance >= self.melhorPerf:
	    #        for i in range(0, 24):
	    #            self.melhoresParam[i] = self.parameters[i]
	    #        self.melhorPerf = performance
            vetorAuxiliar = self.melhoresParam #copia vetor p/ um auxiliar

        # Gera vizinho
            gamma = random.randint(0,7)

        #for i in range(0,len(self.parameters)):
        #    vetorAuxiliar[i] = self.parameters[i] + (random.uniform(-0.001, 0.001))
            vetorAuxiliar[gamma] = self.melhoresParam[gamma] + (random.uniform(-0.1, 0.1))
            vetorAuxiliar[gamma + 8] = self.melhoresParam[gamma + 8] + (random.uniform(-0.1, 0.1))
            vetorAuxiliar[gamma + 16] = self.melhoresParam[gamma + 16] + (random.uniform(-0.1, 0.1))

            self.parameters = vetorAuxiliar # coloca o vetor gerado no vetor que será testado

        # Imprime a fins de teste
            if episode > 0: # and episode % 10 == 0:
	            print "episodio: %d atual: %d melhor: %d" % (episode, self.mediaPerf, self.mediamelhorPerf) + "\n"
	        
	            output = open("./params/Doom%d.txt" % (episode+1), "w+")
	            for i in range(0,24):
	                output.write(str(self.melhoresParam[i]) + "\n")
	                #print "melhoresparam[%d]: " % i
	                #print str(self.melhoresParam[i]) + "\n"

        pass
